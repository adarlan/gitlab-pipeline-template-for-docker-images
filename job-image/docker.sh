#!/bin/bash
set -e

_section () {
    Message="${@}"
    echo "_____________________________________________________________________________"
    echo "${Message^^}"
}

_info () {
    Message="${@}"
    echo "[INFO] ${Message}"
}

_error () {
    Message="${@}"
    echo "[ERROR] ${Message}"
    exit 1
}

_login () {
    # TODO supported platforms
    # GitLab
    # - REGISTRY_URL: $CI_REGISTRY
    # - REGISTRY_USER: $CI_REGISTRY_USER
    # - REGISTRY_PASSWORD: $CI_REGISTRY_PASSWORD
    # AWS ECR
    # GitHub Container Registry
    # Docker Hub
    # Registry URL

    # TODO [ "${REGISTRY_URL}" = "" ] && _error
    # TODO [ "${REGISTRY_USER}" = "" ] && _error
    # TODO [ "${REGISTRY_PASSWORD}" = "" ] && _error

    if [ "$CONTAINER_REGISTRY_TYPE" = "" ]; then
        _section "Resolving CONTAINER_REGISTRY_TYPE"
        [ "$CONTAINER_REGISTRY_TYPE" = "" ] && [ "$DOCKER_HUB_USER" != "" ] && CONTAINER_REGISTRY_TYPE="docker-hub"
        [ "$CONTAINER_REGISTRY_TYPE" = "" ] && [ "$CI_REGISTRY" != "" ] && CONTAINER_REGISTRY_TYPE="gitlab"
        [ "$CONTAINER_REGISTRY_TYPE" = "" ] && _error "Unable resolve CONTAINER_REGISTRY_TYPE"
        _info "CONTAINER_REGISTRY_TYPE: $CONTAINER_REGISTRY_TYPE"
    fi

    if [ "$CONTAINER_REGISTRY_TYPE" = "gitlab" ]; then
        _section "Logging into GitLab Container Registry"
        docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    fi

    if [ "$CONTAINER_REGISTRY_TYPE" = "docker-hub" ]; then
        _section "Logging into Docker Hub Container Registry"
        docker login -u "$DOCKER_HUB_USER" -p "$DOCKER_HUB_PASSWORD"
    fi
}

[ "${CONTEXT}" = "" ] && CONTEXT="."
[ "${DOCKERFILE}" = "" ] && DOCKERFILE="Dockerfile"

DOCKERFILE_PARAM="--file ${DOCKERFILE}"

build-and-scan () {
    _section "Building image for test"
    docker build --pull -t new-image ${DOCKERFILE_PARAM} "${CONTEXT}"
    # TODO what if the FROM uses a private image?

    _section "Scanning for vulnerabilities in the image"
    docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -t aquasec/trivy image new-image
    # docker run --rm -v /var/run/docker.sock:/var/run/docker.sock --entrypoint "" -it aquasec/trivy sh
    # trivy image test
    # TODO grep: Total: 6 (UNKNOWN: 0, LOW: 0, MEDIUM: 6, HIGH: 0, CRITICAL: 0)
    # add variables to define the tolerance
}

build-and-push () {
    _login

    # TODO [ "${IMAGE_PATH}" = "" ] && project path

    [ "${REGISTRY_URL}" = "" ] && IMAGE="${IMAGE_PATH}" \
    || IMAGE="${REGISTRY_URL}/${IMAGE_PATH}"

    _section "Building Docker image: ${IMAGE}"
    _info "DOCKERFILE: $DOCKERFILE"
    _info "CONTEXT: $CONTEXT"
    docker build --pull -t "${IMAGE}" ${DOCKERFILE_PARAM} "${CONTEXT}"
    # TODO allow build-args

    PUSH_EMPTY_TAG="false"
    PUSH_PATCH_LEVEL_TAG="true"
    PUSH_MINOR_LEVEL_TAG="true"
    PUSH_MAJOR_LEVEL_TAG="true"

    if [ "${PUSH_EMPTY_TAG}" = "true" ]; then
        _section "Pushing image: ${IMAGE}"
        docker push "${IMAGE}"
    fi

    if [ "${PUSH_PATCH_LEVEL_TAG}" = "true" ]; then

        # TODO [ "${VERSION_TAG}" = "" ] && _error

        MAJOR_MINOR_PATCH="${VERSION_TAG}"
        ImageTag="${IMAGE}:${MAJOR_MINOR_PATCH}"

        _section "Pushing image: ${ImageTag}"
        docker image tag "${IMAGE}" "${ImageTag}"
        docker push "${ImageTag}"
    fi

    if [ "${PUSH_MINOR_LEVEL_TAG}" = "true" ]; then

        # TODO [ "${VERSION_TAG}" = "" ] && _error

        MAJOR_MINOR="${VERSION_TAG%.*}"
        ImageTag="${IMAGE}:${MAJOR_MINOR}"

        _section "Pushing image: ${ImageTag}"
        docker image tag "${IMAGE}" "${ImageTag}"
        docker push "${ImageTag}"
    fi

    if [ "${PUSH_MAJOR_LEVEL_TAG}" = "true" ]; then

        # TODO [ "${VERSION_TAG}" = "" ] && _error
        
        MAJOR="${VERSION_TAG%%.*}"
        ImageTag="${IMAGE}:${MAJOR}"

        _section "Pushing image: ${ImageTag}"
        docker image tag "${IMAGE}" "${ImageTag}"
        docker push "${ImageTag}"
    fi
}

$@
