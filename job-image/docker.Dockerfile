FROM docker:24-git
RUN apk add bash

COPY docker.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker.sh
